import React, { Component } from "react";

const getOnLineStatus = () => {
  return typeof navigator !== "undefined" &&
    typeof navigator.onLine === "boolean"
    ? navigator.onLine
    : true;
};

class OnlineStatus extends Component {
  state = {
    online: getOnLineStatus(),
    error: false,
  };

  componentDidMount() {
    const { addEventListener } = window;

    addEventListener("online", this.setOnlineStatus);
    addEventListener("offline", this.setOfflineStatus);
  }

  componentWillUnmount() {
    window.removeEventListener("online", this.setOnlineStatus);
    window.removeEventListener("offline", this.setOfflineStatus);
  }

  setOnlineStatus = () => {
    this.setState({ online: true });
  };

  setOfflineStatus = () => {
    this.setState({ online: false });
  };

  throwError = () => {
    this.setState({ error: true });
  };

  render() {
    const { online, error } = this.state;
    if (error) {
      online = false;
    }
    return (
      <h3>
        {online ? (
          <p style={{ color: "green" }}>Online</p>
        ) : (
          <p style={{ color: "red" }}>Offline</p>
        )}

        <button onClick={this.throwError}>ERRORS</button>
      </h3>
    );
  }
}

export default OnlineStatus;
