import React, { Component } from "react";
import ErrorsBoundries from "../ErrorsBoundries/ErrorsBoundries";
import OnlineStatus from "../../components/App/OnlineStatus/OnlineStatus";

class App extends Component {
  // static getDerivedStateFromProps(nextProps, prevState) {
  //  console.log(nextProps, prevState);
  // }

  constructor() {
    super();
    this.state = {
      // count: 1,
      data: [],
      // filter: "all",
      page: 1,
      showMore: false,
      online: navigator.onLine || true,
    };
  }

  componentDidMount() {
    // const { addEventListener } = window;
    window.addEventListener("online", this.setOnlineStatus);
    window.addEventListener("offline", this.setOfflineStatus);

    this.fetchGames();
  }

  fetchGames() {
    const { page, showMore } = this.state;
    fetch(`https://api.rawg.io/api/games?page=${page}`)
      .then((res) => res.json())
      .then((data) => {
        if (showMore) {
          this.setState((prevState) => ({
            data: [...prevState.data, ...data.results],
          }));
        } else {
          this.setState({ data: data.results });
        }
      });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.page !== this.state.page) {
      this.fetchGames();
    }
  }

  setOnlineStatus = () => {
    this.setState({ online: true });
  };

  setOfflineStatus = () => {
    this.setState({ online: false });
  };

  prevPage = () => {
    this.setState((prevState) => ({
      page: prevState.page - 1,
      showMore: false,
    }));
  };

  nextPage = () => {
    this.setState((prevState) => ({
      page: prevState.page + 1,
      showMore: false,
    }));
  };

  showMore = () => {
    this.setState((prevState) => ({
      page: prevState.page + 1,
      showMore: true,
    }));
  };

  componentWillUnmount() {
    const { removeEventListener } = window;
    removeEventListener("online", this.setOnlineStatus);
    console.log("removeEventListener", "online");
    removeEventListener("offline", this.setOfflineStatus);
    console.log("removeEventListener", "offline");
  }

  render() {
    // const visible = this.search(this.filter(this.state.todos, this.state.filter), 'search');
    // const step = 1;
    // return <h1>Hello World! {step === 2 ? <p>Hello</p> : <p>Buy</p>}</h1>;
    const { online, data } = this.state;
    // console.log(page);
    const items = data.map(({ id, name }) => (
      <li key={id.toString()}>{name}</li>
    ));

    // const isOnline = online ? (
    //   <span style={{ color: "green" }}>Online</span>
    // ) : (
    //   <span style={{ color: "red" }}>Offline</span>
    // );
    return (
      <div className="container">
        {/* {isOnline} */}
        <ErrorsBoundries>
          <ul>{items}</ul>
          <button onClick={this.prevPage}>prev</button>
          <button onClick={this.showMore}>more...</button>
          <button onClick={this.nextPage}>next</button>
        </ErrorsBoundries>

        <ErrorsBoundries>
          <OnlineStatus />
        </ErrorsBoundries>
      </div>
    );
  }
}

export default App;
