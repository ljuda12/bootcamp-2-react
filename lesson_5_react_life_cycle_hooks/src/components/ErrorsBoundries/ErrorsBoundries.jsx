import React, { Component } from "react";
import ErrorsIndicator from "../../shared/components/ErrorsIndicator";

class ErrorsBoundries extends Component {
  state = {
    hasError: false,
  };

  componentDidCatch(error, info) {
    console.log(error, info);
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return <ErrorsIndicator />; // Ошибка рендера обновите страницу
    }
    return this.props.children;
  }
}

export default ErrorsBoundries;
