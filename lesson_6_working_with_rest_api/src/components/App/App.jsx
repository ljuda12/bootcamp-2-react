import React from "react";
import List from "../List";

const App = () => {
  const url = "https://api.rawg.io/api/games";
  return <List />;
};

export default App;
