import React from "react";
import PropTypes from "prop-types";

import Container from "../../shared/components/Container";
import Row from "../../shared/components/Row";
import Card from "../Card/Card";

const List = ({ data }) => {
  const items = data.map((item) => item);
  return (
    <Container>
      <Row>
        <Card />
      </Row>
    </Container>
  );
};

List.defaultProps = {
  data: [],
};

List.propTypes = {
  data: PropTypes.array.isRequired,
};

export default List;
