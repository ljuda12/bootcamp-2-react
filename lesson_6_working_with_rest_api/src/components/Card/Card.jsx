import React from "react";
import PropTypes from "prop-types";

const Card = ({ name, background_image, metacritic }) => {
  return (
    <div className="col-md-4">
      <div className="card">
        <img
          src={background_image}
          className="products-card-image"
          alt={name}
        />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <strong className="text-center">{metacritic}</strong>
          <button className="btn btn-primary">More...</button>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  name: PropTypes.string.isRequired,
  background_image: PropTypes.string.isRequired,
  metacritic: PropTypes.number.isRequired,
};

export default Card;
