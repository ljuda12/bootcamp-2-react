import React from "react";
import PropTypes from "prop-types";

const Section = ({ title, children }) => {
  return (
    <section className="section-wrapper">
      <h1 className="section-title">{title}</h1>
      {children}
    </section> 
  );
};

Section.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default Section;
