import React from "react";
import PropTypes from "prop-types";
import './Column.css';

const Column = ({ children, column }) => {
  const className = `col col-${column}`;

  return <div className={className}>{children}</div>;
};

Column.defaultProps = {
  column: 1,
};

Column.propTypes = {
  column: PropTypes.number,
  children: PropTypes.node.isRequired,
};

export default Column;
