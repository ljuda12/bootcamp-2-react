import React from "react";
import PropTypes from "prop-types";

import classes from "./Button.module.scss";

const Button = ({ children }) => {
  const { Button, ButtonPrimary } = classes;
  return <button className={`${Button}  ${ButtonPrimary}`}>{children}</button>;
};

Button.propTypes = {
  children: PropTypes.node,
};

export default Button;
