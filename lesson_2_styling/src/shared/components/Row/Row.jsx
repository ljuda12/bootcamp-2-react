import React from "react";
import PropTypes from "prop-types";
import "./Row.css";

const Row = ({ children }) => {
  return <div className="raw">{children}</div>;
};

Row.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Row;
