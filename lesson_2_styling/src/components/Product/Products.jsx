import React from "react";
import PropTypes from "prop-types";
import { Title, Image } from "./product.styled";

import Button from "../../shared/components/Button";


const Product = ({ price, image, title, description, buttonText }) => {
  return (
    <div className="card">
      <Image src={image} alt={title} />
      <Title disabled={!!title}>{title}</Title>
      <p>{description}</p>
      <span className="price">{price}</span>
      <Button>{buttonText}</Button>
    </div>
  );
};

Product.defaultProps = {
  buttonText: "Buy",
};

Product.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  buttonText: PropTypes.string,
};

export default Product;
