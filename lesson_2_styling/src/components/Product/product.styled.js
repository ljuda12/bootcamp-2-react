import styled, { css } from "styled-components";

const Image = styled.img`
  max-width: 100%;
  width: 100%;
  height: auto;
`;

const Title = styled.h5`
  font-size: 1.5rem;
  font-weight: 700;
  color: gray;
  ${(props) =>
    props.primary &&
    css`
      background: blue;
      color: black;
    `};
  ${(props) =>
    props.danger &&
    css`
      background: red;
      color: black;
    `};
  ${(props) =>
    props.disabled &&
    css`
      background: #ddd;
      color: black;
    `};
`;

export { Image, Title };
