import React from "react";

import Product from "../Product";
import Container from "../../shared/components/Container";
import Row from "../../shared/components/Row";
import Column from "../../shared/components/Column";

import data from "../../assets/mock.json";
import '../../shared/styles/global.scss';


/*
    #TODOS

    1. styled Container component and row
    2.
    3.
    4.
    5.
    6.
*/

const App = () => {
  const items = data.map((item) => (
    <Column column={4} key={item.id}>
      <Product buttonText="By on Click" {...item} />
    </Column>
  ));

  return (
    <Container>
      <Row>{items}</Row>
    </Container>
  );
};

export default App;
