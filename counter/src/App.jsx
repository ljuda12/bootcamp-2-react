import React, { Component } from "react";

class App extends Component {
  state = {
    count: 0,
    term: "",
  };

  handleIncrement = () => {
    this.setState((state) => ({
      count: state.count + 1,
    }));
  };

  handleDecrement = () => {
    if (this.state.count) {
      this.setState((state) => ({
        count: state.count - 1,
      }));
    }
  };

  handleChange = (event) => {
    // console.log(event, "EVENT");
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value });
    // console.log(name, "NAME");
    // console.log(value, "VALUE");
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const term = Number(this.state.term);
    this.setState((state) => {
      return {
        count: state.count + term,
      };
    });
  };

  render() {
    const { count, term } = this.state;

    return (
      <div>
        <h3>Count {count}</h3>
        <button onClick={this.handleIncrement}>+</button>
        <button onClick={this.handleDecrement}>-</button>
        <form onSubmit={this.handleSubmit}>
          <input
            name="term"
            type="number"
            onChange={this.handleChange}
            value={term}
          />
          <button type="submit">Go</button>
        </form>
      </div>
    );
  }
}

export default App;
