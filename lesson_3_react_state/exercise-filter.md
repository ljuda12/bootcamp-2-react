1. Реализовать метод filter в компоненте App; 
2. Cоздать обработчик onFilterChange публичным методом класса.
3. Создать компонент StatusFilter, папка с трёмя файлами подготовлена: 
StatusFilter.jsx, index.js, StatusFilter.module.scss - вёрстка присутствует,
компонент должен быть функциональным, импортируйте scss модуль, 
Принимать на вход он должен следующие props filter - активный фильтр, handleFilterChange - обработчик.