import React from "react";
import Container from "../../shared/components/Container";
import SignIn from "../SignIn";
// import SignUpForm from "../SignUpForm";

const App = () => {
  return (
    <Container>
      <SignIn />
      {/* <SignUpForm /> */}
    </Container>
  );
};

export default App;
