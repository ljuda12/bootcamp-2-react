import React, { Component } from "react";

const genderType = {
  MALE: "male",
  FEMALE: 'female',
};

 
class SignUpForm extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    aggre: true,
    age: "",
    gender: "",
  };

  handleChange = (event) => {
    const name = event.target.name;
    const value =
      event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value;

    this.setState({ [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (!this.state.age) {
      return;
    }
    alert(JSON.stringify(this.state, null, 2));
  };

  render() {
    const { username, email, password, aggre, age, gender } = this.state;

    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          name="username"
          onChange={this.handleChange}
          value={username}
        />
        <input
          type="email"
          name="email"
          onChange={this.handleChange}
          value={email}
        />
        <input
          type="password"
          name="password"
          onChange={this.handleChange}
          value={password}
        />
        <input
          name="aggre"
          type="checkbox"
          onChange={this.handleChange}
          checked={aggre}
        />

        <input
          type="radio"
          name="gender"
          checked={gender === genderType.MALE}
          value="male"
          onChange={this.handleChange}
        />

        <input
          type="radio"
          name="gender"
          checked={gender === genderType.FEMALE}
          value="female"
          onChange={this.handleChange}
        />

        <select name="age" onChange={this.handleChange} value={age}>
          <option value="" disabled>
            not selected
          </option>
          <option value="18-25">18-25</option>
          <option value="26-35">26-35</option>
          <option value="36+">36+</option>
        </select>

        <button type="submit">SignUp</button>
      </form>
    );
  }
}

export default SignUpForm;
