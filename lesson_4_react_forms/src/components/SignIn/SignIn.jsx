import React from "react";
import EasyForm from "../EasyForm";
import fields from "../../assets/register-fields";
import FormControl from "../dont-touch/FormControl/FormControl";

const SignIn = () => {
  return (
    <EasyForm
      // Как помним это значение по умолчанию для нашей формы.
      initialValues={{ username: "", email: "", age: "", agree: false }}
      // это тот самы пропс в который мы закидываем состояние values,
      // и в нём у нас есть доступ к значением полей
      onSubmit={(values) => {
        // простой и не сложный debug
        alert(JSON.stringify(values, null, 2));
      }}
    >
      {/*Начало render функции, вспоминаем что мы передали в качестве children для EasyForm, функцию и сейчас мы нее пользуемся  */}
      {({ values, handleChange, handleSubmit }) => (
        // добавляем handleSubmit, когда он сработает он передаст даные из формы в onSubmit props, который више.
        <form onSubmit={handleSubmit}>
          {/* Делаем map по полям, трансформируем в нужную нам разметку */}
          {fields.map((field) => {
            return (
              <FormControl
                // name, есть не везде упустил этот момент, потому добавил простую проверку
                // если field.name undefined то запиши field.label в качестве ключа
                key={field.name || field.label}
                // наш handleChange
                handleChange={handleChange}
                // value для привязки инпута генерируем,
                // Так как values это объект мы можем обратиться к нему динамически через values[field.name]
                // name это уникальное название поля которые
                // мы передаем при мапинге то есть это аналогично тому мы писали каждому полю values.email и тд. !
                value={values[field.name]}
                {...field}
              />
              //   <div key={field.name}>
              //     <label htmlFor={field.id}>{field.label}</label>
              //     <input
              //       id={field.id}
              //       name={field.name}
              //       type={field.type}
              //       handleChange={handleChange}
              //       value={values[field.name]}
              //     />
              //   </div>
            );
          })}
          {/* <input
            type="text"
            name="username"
            onChange={handleChange}
            value={values.username}
          />
          <input
            type="email"
            name="email"
            onChange={handleChange}
            value={values.email}
          /> */}
        </form>
        // окончание render функции
      )}
    </EasyForm>
  );
};

export default SignIn;
