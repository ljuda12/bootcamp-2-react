import { Component } from "react";

class EasyForm extends Component {
  state = {
    // значение по умолчанию для values, передаеться из props. Если не передали возвращаем, пустой объект.
    values: this.props.initialValues || {},
    // К сожалению не успели дописать, должно зраниться состояние трогали поле или нет
    touched: {},
  };

  // handleChange for input and selects 
  handleChange = (event) => {
    const name = event.target.name; // здесь храниться ключь и значения из name пропа
    const value = // храним результат выполнения выражения
      event.target.type === "checkbox" // Проверяем если тип инпута checkbox  
        ? event.target.checked // возвращаем для него состояние checked булевое true/false 
        : event.target.value // если не ckeckbox, возвращаем value;
    // event.persist(); нужен только для React 16, если не указать код может сломаться. 
    this.setState((state) => {
      // состояние основано на преидущем, вызываем функцию обратного вызова
      // возврощаем объект values: { ...state.values, [name]: value  } 
      return {
        values: {
          ...state.values,
          [name]: value,
        },
      };
    });
  };

  handleSubmit = (event) => {
    // Отменяем двействие формы по умолчанию, то есть переход и перезагрузка страницы.  
    event.preventDefault();

    // Вызываем onSubmit и передаем в его аргументы наш this.state.values как props
    this.props.onSubmit(this.state.values);
  };

  render() {
    // Возвращаем children как функцию и передаем полезную нагрузку в качестве state и наших обработчиков. 
    return this.props.children({
      ...this.state,
      handleChange: this.handleChange,
      handleSubmit: this.handleSubmit,
    });
  }
}

export default EasyForm;
