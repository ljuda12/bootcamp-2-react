import React, { memo } from "react";

// Типы полей которые пока что поддержывает наш компонент 
const inputType = {
  TEXT: "text",
  PASSWORD: "password",
  SELECT: "select",
  CHECKBOX: "checkbox",
  SUBMIT: "submit",
};

/* FormControl 
    Простой компонент со switch который сверяет inputType, 
    с тем типом который мы передаем во время мапинга,
    если разметка для какого-то поля отличаеться, мы можем внести изменения, 
    handleBlur пока что не работает.
*/
const FormControl = ({
  label,
  name,
  type,
  id,
  options,
  value,
  handleChange,
  handleBlur,
}) => {
  const { TEXT, SELECT, CHECKBOX, SUBMIT, PASSWORD } = inputType;
  // Деструктируем наши типы, помним что мы сверяемся по строкам то есть TEXT = равен 'text' 
  switch (type) {
    case TEXT:
      return (
        <div className="mb-3 mt-3 ">
          <label htmlFor={id} className="form-label">
            {label}
          </label>
          <input
            type="text"
            className="form-control"
            name={name}
            id={id}
            onChange={handleChange}
            onBlur={handleBlur}
            value={value}
          />
        </div>
      );
    case PASSWORD:
      return (
        <div className="mb-3 mt-3">
          <label htmlFor={id} className="form-label">
            {label}
          </label>
          <input
            type="password"
            className="form-control"
            name={name}
            id={id}
            onChange={handleChange}
            onBlur={handleBlur}
            value={value}
          />
        </div>
      );
    case CHECKBOX:
      return (
        <div className="form-check mt-3">
          <label className="form-check-label" htmlFor={id}>
            {label}
          </label>
          <input
            name={name}
            className="form-check-input"
            type={type}
            checked={value}
            id={id}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
      );
    case SELECT:
      return (
        <select
          name={name}
          className="form-select mt-3"
          aria-label={id}
          value={value}
          onChange={handleChange}
        >
          {options.map(({ label, value }) => (
            <option value={value} key={value}>
              {label}
            </option>
          ))}
        </select>
      );
    case SUBMIT:
      return (
        <button type="submit" className="btn btn-primary mt-3">
          {label}
        </button>
      );
    default:
      return (
        <div className="form-group mt-3">
          <label htmlFor={id} className="form-label">
            {label}
          </label>
          <input
            type="text"
            name={name}
            className="form-control"
            id={id}
            onChange={handleChange}
            onBlur={handleBlur}
            value={value}
          />
        </div>
      );
  }
};

export default memo(FormControl);
