import shortid from "shortid";

// Убрал константу и сделал вызов для каждого id, отдельно. 

const registerFields = [
  {
    type: "text",
    name: "username",
    id: shortid.generate(),
    label: "UserName: ",
  },
  {
    name: "email",
    type: "email",
    id: shortid.generate(),
    label: "Email: ",
  },
  {
    name: "password",
    type: "password",
    id: shortid.generate(),
    label: "Password: ",
  },
  {
    name: "agree",
    type: "checkbox",
  },
  {
    name: "age",
    type: "select",
    options: [
      {
        label: "Select your age",
        value: "",
        disabled: true,
      },
      {
        label: "18-25",
        value: "18-25",
      },
      {
        label: "26-35",
        value: "26-35",
      },
      {
        label: "36+",
        value: "36+",
      },
    ],
  },
    {
    type: "submit",
    label: "Sign Up",
  },
];

export default registerFields;
